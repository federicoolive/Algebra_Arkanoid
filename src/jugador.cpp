#include "jugador.h"
#include "configuraciones.h"

namespace Jugador
{
	JUGADOR player;

	void init()
	{
		player.position = { screenWidth / 2, screenHeight * screenMeasurement };
		player.size = { screenWidth / 10 , playerHeight };
		player.life = jugadorMaxVida;
	}

	void update()
	{

	}

	void draw()
	{
		DrawRectangle(static_cast<int>(player.position.x - player.size.x / 2), static_cast<int>(player.position.y - player.size.y / 2), static_cast<int>(player.size.x), static_cast<int>(player.size.y), BLACK);
	}

	void input()
	{
		if (IsKeyDown(KEY_LEFT))
		{
			player.position.x -= 5;
		}

		if (player.position.x - player.size.x / 2 <= 0)
		{
			player.position.x = player.size.x / 2;
		}

		if (IsKeyDown(KEY_RIGHT))
		{
			player.position.x += 5;
		}

		if (player.position.x + player.size.x / 2 >= screenWidth)
		{
			player.position.x = screenWidth - player.size.x / 2;
		}
	}

	void deinit()
	{

	}
}