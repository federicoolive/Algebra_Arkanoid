#include "pelota.h"
#include "configuraciones.h"
#include "jugador.h"

namespace Pelota
{
	Ball ball;

	void init()
	{
		ball.position = { screenWidth / 2, screenHeight * screenMeasurement - playerHeight - 10 };
		ball.speed = { 0, 0 };
		ball.radius = 7;
		ball.active = false;
	}

	void update()
	{
		if (ball.active)
		{
			ball.speed.y += Config::gravedad;
			ball.position.x += ball.speed.x;
			ball.position.y += ball.speed.y;
		}
		else
		{
			ball.position = { Jugador::player.position.x, screenHeight * 7 / 8 - 30 };
		}

		if (ball.position.x + ball.radius >= screenWidth || ball.position.x - ball.radius <= 0)
		{
			ball.speed.x *= -1;
		}

		if ((ball.position.y - ball.radius) <= 0)
		{
			ball.speed.y *= -1;
		}

		if (ball.position.y + ball.radius >= screenHeight)
		{
			ball.speed = { 0, 0 };
			ball.active = false;

			Jugador::player.life--;
		}
	}

	void draw()
	{
		DrawCircleV(ball.position, ball.radius, BLUE);
		DrawLine(static_cast<int>(ball.position.x), static_cast<int>(ball.position.y), static_cast<int>(ball.position.x + ball.speed.x * 100), static_cast<int>(ball.position.y + ball.speed.y * 100), GREEN);
	}

	void input()
	{
		if (!ball.active)
		{
			if (IsKeyPressed(KEY_SPACE))
			{
				ball.active = true;
				ball.speed.x = 1.5f;
				ball.speed.y = -3;
			}
		}
	}

	void deinit()
	{

	}
}