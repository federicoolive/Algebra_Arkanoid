#ifndef PELOTA_H
#define PELOTA_H

#include "raylib.h"

namespace Pelota
{
    struct Ball {
        bool active;
        Vector2 position;
        Vector2 speed;
        float radius;
    };
    extern Ball ball;

    void init();
    void update();
    void draw();
    void input();
    void deinit();
}

#endif