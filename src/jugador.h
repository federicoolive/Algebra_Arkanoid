#ifndef JUGADOR_H
#define JUGADOR_H

#include "raylib.h"

namespace Jugador
{
    struct JUGADOR {
        bool active = true;
        Vector2 position;
        Vector2 size;
        int life;
    };
    extern JUGADOR player;

    void init();
    void update();
    void draw();
    void input();
    void deinit();

}

#endif