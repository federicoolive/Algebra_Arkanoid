#ifndef GAME_H
#define GAME_H

#include "raylib.h"
#include "jugador.h"
#include "pelota.h"
#include "bloque.h"

namespace game
{
	void init();
	void play();
	void input();
	void update();
	void draw();
	void deinit();

	bool chequearColisionBorde(Vector2 posicionPel, float radio, Rectangle rec);
	float distanciaEntrePuntos(Vector2 p1, Vector2 p2);
}

#endif