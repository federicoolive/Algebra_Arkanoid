#ifndef BLOQUE_H
#define BLOQUE_H

#include "raylib.h"
#include "configuraciones.h"

namespace Bloque
{
    struct Brick {
        bool active;
        Vector2 position;
    };
    extern Brick brick[maxFil][maxCol];

    void init();
    void update();
    void draw();
    void deinit();
}

#endif