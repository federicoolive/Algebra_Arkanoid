#ifndef CONFIGURACIONES_H
#define CONFIGURACIONES_H

#include "raylib.h"

namespace Config
{
	const int screenWidth = 800;
	const int screenHeight = 450;

	const int  jugadorMaxVida = 5;
	const int  maxFil = 5;
	const int  maxCol = 20;
	const int  OUTPUTX = 5;
	const int  OUTPUTY = 5;

	const float screenMeasurement = 7 / 8.f;
	const int playerHeight = 20;
	const float anguloMax = 55;
	const float gravedad = 0.0135f;

	extern bool gameOver;
	extern bool pause;

	extern float hipotenusa;
	extern float golpePelotaBarra;
	extern float anguloDireccionInicial;
	extern float anguloDireccionFinal;

	extern Vector2 brickSize;
}
using namespace Config;

#endif