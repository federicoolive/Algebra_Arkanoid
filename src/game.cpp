#include "game.h"
#include <iostream>
#include "configuraciones.h"
#include "jugador.h"
#include "pelota.h"
#include "bloque.h"

namespace game
{
    void init()
    {
        Jugador::init();
        Pelota::init();
        Bloque::init();
    }

    void play()
    {
        InitWindow(screenWidth, screenHeight, "AR_kanoid"); 
        SetTargetFPS(60);
        InitAudioDevice();
        
        init();

        while (!WindowShouldClose())
        {
            update();
            input();
            draw();
        }

        deinit();
        CloseWindow();
    }

    void input()
    {
        Jugador::input();
        Pelota::input();
    }

    void update()
    {
        using namespace Pelota;
        using namespace Jugador;
        using namespace Bloque;

        if (!gameOver)
        {
            if (IsKeyPressed('p') || IsKeyPressed('P')) pause = !pause;

            if (!pause)
            {
                input();

                Pelota::update();

                //player.position.x = ball.position.x;    // Aim
                if (CheckCollisionCircleRec(ball.position, ball.radius, { player.position.x - player.size.x / 2, player.position.y - player.size.y / 2, player.size.x, player.size.y }))
                {                                                                                               // IMPORTANTE: Los (* 180 / PI) son porque la mierda math.h trabaja con radianes
                    if (ball.speed.y > 0)   // Para que solo choque cuando la pelota va hacia abajo
                    {
                        std::cout << "----------------------------------------" << std::endl;

                        // ----- REBOTE NORMAL -----
                        hipotenusa = sqrtf(powf(ball.speed.y, 2) + powf(ball.speed.x, 2));                              // Se vuelve a calcular porque es afectada po la gravedad
                        std::cout << "Hipotenusa:             " << hipotenusa << std::endl;

                        anguloDireccionInicial = fabsf(asinf(ball.speed.y / hipotenusa) * 180 / PI);                    // Se calcula el �ngulo con el que viene la pelota

                        std::cout << "Angulo Inicial:         " << anguloDireccionInicial << std::endl;
                        
                        // Angulo final: Depende su signo de la direcci�n de la pelota
                        if (ball.speed.x < 0)
                            anguloDireccionFinal = -fabsf(anguloDireccionInicial);
                        else if (ball.speed.x > 0)
                            anguloDireccionFinal = fabsf(anguloDireccionInicial);
                        
                        std::cout << "Angulo Result:          " << anguloDireccionFinal << std::endl;

                        ball.speed.y = -fabsf(sinf(anguloDireccionFinal * PI / 180) * hipotenusa);
                        ball.speed.x = (cosf(anguloDireccionFinal * PI / 180) * hipotenusa);
                        
                        if (anguloDireccionFinal < 0) ball.speed.x *= -1;
                        
                        std::cout << "Y':                     " << ball.speed.y << std::endl;
                        std::cout << "X':                     " << ball.speed.x << std::endl;

                        // ----- SUMA DE MODIFICACI�N DE BARRA -----
                        golpePelotaBarra = (anguloMax * (ball.position.x - player.position.x)) / (player.size.x / 2);   // �ngulo que modifica S/ donde impacte

                        if (golpePelotaBarra > anguloMax) golpePelotaBarra = anguloMax;
                        else if (golpePelotaBarra < -anguloMax) golpePelotaBarra = -anguloMax;
                        
                        std::cout << "Modificion de la Barra: " << golpePelotaBarra << std::endl;

                        if (ball.position.x < player.position.x) // Golpe en posicion de la Pelota con respecto a la Barra
                        {
                            if (ball.speed.x > 0)
                            {
                                golpePelotaBarra = -golpePelotaBarra;
                            }

                            anguloDireccionFinal += golpePelotaBarra;
                            if (anguloDireccionFinal < 35 && ball.speed.x < 0)
                            {
                                anguloDireccionFinal = -35;
                            }
                        }
                        else
                        {
                            anguloDireccionFinal -= golpePelotaBarra;
                            if (anguloDireccionFinal < 35 && ball.speed.x > 0)
                                anguloDireccionFinal = 35;
                        }
                        std::cout << "Direccion Final:        " << anguloDireccionFinal << std::endl;

                        if (anguloDireccionFinal < 35 && anguloDireccionFinal > 0)
                            anguloDireccionFinal = 35;
                        else if (anguloDireccionFinal > -35 && anguloDireccionFinal < 0)
                            anguloDireccionFinal = -35;

                        std::cout << "Direccion Final +-90:   " << anguloDireccionFinal << std::endl;

                        ball.speed.y = -fabsf(sinf(anguloDireccionFinal * PI / 180) * hipotenusa);
                        ball.speed.x = (cosf(anguloDireccionFinal * PI / 180) * hipotenusa);
                        
                        if (anguloDireccionFinal < 0) ball.speed.x *= -1;
                        
                        std::cout << "Y':                     " << ball.speed.y << std::endl;
                        std::cout << "X':                     " << ball.speed.x << std::endl;
                        
                        pause = pause;  // CAMBIAR PAUSA
                    }
                }

                // Colision Pelota Brick.
                for (int i = 0; i < maxFil; i++)
                {
                    for (int j = 0; j < maxCol; j++)
                    {
                        if (brick[i][j].active)
                        {
                            Rectangle rec = { 0 };
                            rec.x = brick[i][j].position.x - brickSize.x / 2;
                            rec.y = brick[i][j].position.y - brickSize.y / 2;
                            rec.width = brickSize.x;
                            rec.height = brickSize.y;

                            if (chequearColisionBorde(ball.position, ball.radius, rec))
                            {
                                // A | B | C    1
                                // D | - | E    3
                                // F | G | H    2
                                char Case = 0;
                                if (ball.position.y < brick[i][j].position.y - brickSize.y / 2)
                                {
                                    if (ball.position.x < brick[i][j].position.x - brickSize.x / 2)
                                    {
                                        Case = 'A';
                                    }
                                    else if (ball.position.x < brick[i][j].position.x + brickSize.x / 2)
                                    {
                                        Case = 'C';
                                    }
                                    else
                                    {
                                        Case = 'B';
                                    }
                                }
                                else if (ball.position.y > brick[i][j].position.y + brickSize.y / 2)
                                {
                                    if (ball.position.x < brick[i][j].position.x - brickSize.x / 2)
                                    {
                                        Case = 'F';
                                    }
                                    else if (ball.position.x < brick[i][j].position.x + brickSize.x / 2)
                                    {
                                        Case = 'G';
                                    }
                                    else
                                    {
                                        Case = 'H';
                                    }
                                }
                                else
                                {
                                    if (ball.position.x < brick[i][j].position.x + brickSize.x / 2)
                                    {
                                        Case = 'D';
                                    }
                                    else 
                                    {
                                        Case = 'E';
                                    }
                                }

                                switch (Case)
                                {
                                case 'A':
                                    ball.speed.x = -fabsf(ball.speed.x);
                                    ball.speed.y = -fabsf(ball.speed.x);
                                    break;
                                case 'B':
                                case 'G':
                                    ball.speed.y = -ball.speed.y;
                                    break;
                                case 'C':
                                    ball.speed.x = fabsf(ball.speed.x);
                                    ball.speed.y = -fabsf(ball.speed.x);
                                    break;
                                case 'D':
                                case 'E':
                                    ball.speed.x = -ball.speed.x;
                                    break;
                                case 'F':
                                    ball.speed.x = -fabsf(ball.speed.x);
                                    ball.speed.y = fabsf(ball.speed.x);
                                    break;
                                case 'H':
                                    ball.speed.x = fabsf(ball.speed.x);
                                    ball.speed.y = fabsf(ball.speed.x);
                                    break;
                                default:
                                    break;
                                }
                                brick[i][j].active = false;
                                std::cout << "Caso de Colision: " << Case << std::endl;
                            }
                        }
                    }
                }

                if (player.life <= 0)
                {
                    gameOver = true;
                }
                else
                {
                    gameOver = true;

                    for (int i = 0; i < maxFil; i++)
                    {
                        for (int j = 0; j < maxCol; j++)
                        {
                            if (brick[i][j].active) gameOver = false;
                        }
                    }
                }
            }
        }
        else
        {
            if (IsKeyPressed(KEY_ENTER))
            {
                init();
                gameOver = false;
            }
        }
    }

    void draw()
    {
        BeginDrawing();
        ClearBackground(RAYWHITE);

        if (!gameOver)
        {
            for (int i = 0; i < Jugador::player.life; i++)   // HUD DE VIDA
            {
                DrawRectangle(20 + 40 * i, screenHeight - 30, 35, 10, RED);
            }

            Jugador::draw();
            Pelota::draw();
            Bloque::draw();
            
            if (pause)
            {
                DrawText("GAME PAUSED", screenWidth / 2 - MeasureText("GAME PAUSED", 40) / 2, screenHeight / 2 - 40, 40, GRAY);
            }
        }
        else
        {
            DrawText("PRESS [ENTER] TO PLAY AGAIN", GetScreenWidth() / 2 - MeasureText("PRESS [ENTER] TO PLAY AGAIN", 20) / 2, GetScreenHeight() / 2 - 50, 20, GRAY);
        }

        EndDrawing();
    }

    void deinit()
    {
        Jugador::deinit();
        Pelota::deinit();
        Bloque::deinit();

    }

    float distanciaEntrePuntos(Vector2 p1, Vector2 p2)
    {
        return sqrtf(powf((p1.x - p2.x), 2) + powf((p1.y - p2.y), 2)); // Pitagoras
    }

    bool chequearColisionBorde(Vector2 posicionPel, float radio, Rectangle rec)
    {
        Vector2 p = posicionPel;

        if (p.x < rec.x)
        {
            p.x = rec.x;
        }
        else if (p.x > rec.x + rec.width)
        {
            p.x = rec.x + rec.width;
        }

        if (p.y < rec.y)
        {
            p.y = rec.y;
        }
        else if (p.y > rec.y + rec.height)
        {
            p.y = rec.y + rec.height;
        }

        float distancia = distanciaEntrePuntos(posicionPel, p);
        if (distancia < radio)
        {
            return true;
        }
        return false;
    }
}