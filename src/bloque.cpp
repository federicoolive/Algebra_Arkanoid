#include "bloque.h"

namespace Bloque
{
	Brick brick[maxFil][maxCol];

	void init()
	{
		int initialDownPosition = 50;

		brickSize = { (float)(GetScreenWidth() / maxCol), 40.0f };

		for (int i = 0; i < maxFil; i++)
		{
			for (int j = 0; j < maxCol; j++)
			{
				brick[i][j].position = { j * brickSize.x + brickSize.x / 2, i * brickSize.y + initialDownPosition };
				brick[i][j].active = true;
			}
		}
	}

	void update()
	{

	}

	void draw()
	{
		for (int i = 0; i < maxFil; i++)
		{
			for (int j = 0; j < maxCol; j++)
			{
				if (Bloque::brick[i][j].active)
				{
					Color color = BLACK;
					if ((i + j) % 2 == 0) 
						color = PINK;
					else 
						color = SKYBLUE;

					DrawRectangle(static_cast<int>(brick[i][j].position.x - brickSize.x / 2), static_cast<int>(brick[i][j].position.y - brickSize.y / 2), static_cast<int>(brickSize.x), static_cast<int>(brickSize.y), color);
				}
			}
		}
	}

	void deinit()
	{

	}
}